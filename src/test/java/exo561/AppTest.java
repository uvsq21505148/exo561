package exo561;

import static org.junit.Assert.*;
import org.junit.Test;


public class AppTest {
	
	@Test
	public void testRun(){
	}
	
	@Test
    public void testAdresseIP()
    {
		AdresseIP adresse1 = new AdresseIP("192.169.0.1");
		 assertTrue("L'adresse IP n'est pas créer correctement" , adresse1.getName() == "192.169.0.1");
    }
    
    @Test
    public void testNQM() // NQM = Nom Qualifier de Machine
    {
		NomMachine nqm1 = new NomMachine("machine" ,"domaine.local");
		 assertTrue("NQM n'est pas créer correctement" , nqm1.getName().equals("machine.domaine.local"));
		 assertTrue("NQM n'est pas créer correctement" , nqm1.getMachine() == "machine");
		 assertTrue("NQM n'est pas créer correctement" , nqm1.getDomaine() == "domaine.local");
		 
		 
    }
}
