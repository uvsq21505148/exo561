package exo561;
import java.util.ArrayList;
import java.util.*;
import java.util.Date;
import java.io.*;


public class Dns {
	
	ArrayList<Tuples> listeTuples = new ArrayList<Tuples>() ;
	
	public Dns() {
		try{ 
			Properties prop = new Properties();
			FileInputStream in = new FileInputStream("config.properties");
			prop.load(in);
			String nomFichier = prop.getProperty("nomFichier");
			in.close();
			FileReader c = new FileReader(nomFichier);
            BufferedReader r = new BufferedReader(c);
 
            String line = r.readLine();
       
            while (line != null) {
				String[] decompose = line.split(" ");      
				String[] nqm = decompose[0].split("_");
				listeTuples.add(new Tuples(new AdresseIP(decompose[1]), new NomMachine(nqm[0],nqm[1]+ "." + nqm[2])));
				
				 line = r.readLine();
            }
 
            r.close();
		}
		catch (IOException e)  
		{
			e.printStackTrace();
		}
	}
	
	
	public DnsItem getItem(AdresseIP ip1) {
		int i;
		for(i=0;i<listeTuples.size() ; i++){
			if(listeTuples.get(i).getIP().getName().equals(ip1.getName()))
				break;
			
		}
		if(i == listeTuples.size())
			return null;
		else
			return listeTuples.get(i).getNomMachine();
	}
	public DnsItem getItem(NomMachine nm1) {
		int i;
		for(i=0;i<listeTuples.size() ; i++){
			if(listeTuples.get(i).getNomMachine().getName().equals(nm1.getName()))
				break;
			
		}
		if(i == listeTuples.size())
			return null;
		else
			return listeTuples.get(i).getIP();
	}
	
	public ArrayList<NomMachine> getItems(String nomdedomaine, boolean optiontrie) {
		int i;
		ArrayList<NomMachine> listeMachine = new ArrayList<NomMachine>() ;
		listeTuples.sort(new TuplesComparator(optiontrie));
		
		for(i=0;i<listeTuples.size() ; i++){
			if(listeTuples.get(i).getNomMachine().getDomaine().equals(nomdedomaine))
				listeMachine.add(listeTuples.get(i).getNomMachine());
		}
		return listeMachine;
	}
}
