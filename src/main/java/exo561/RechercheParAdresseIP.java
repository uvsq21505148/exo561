package exo561;

public class RechercheParAdresseIP implements Commande
{
	private AdresseIP adresseIP;
	
	public RechercheParAdresseIP(AdresseIP adresseIP) {
			this.adresseIP = adresseIP;
	}
	
	public String execute(Dns serveur) {
		DnsItem tmp = serveur.getItem(adresseIP);
		if(tmp == null)
			return "AdresseIP non trouvée";
		else
			return tmp.getName();
	}
}
