package exo561;

public class RechercheParNomMachine implements Commande
{
	private NomMachine nomMachine;
	
	public RechercheParNomMachine(NomMachine nomQualifieMachine) {
		this.nomMachine = nomMachine;
	}
	
	public String execute(Dns serveur) {
		DnsItem tmp = serveur.getItem(nomMachine);
		if(tmp == null)
			return "Nom qualifié de machine non trouvé";
		else
			return tmp.getName();
	}
}
