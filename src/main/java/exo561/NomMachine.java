package exo561;

public class NomMachine extends DnsItem{
	
	private String machine; // NQM = Nom Qualifier de Machine
	private String domainelocal;
	
	public NomMachine(String mach, String domainel)
	{
		this.machine = mach;
		this.domainelocal = domainel;
	}
	@Override
	public String getName() // Nom Complet 
	{
		return this.machine + "." + this.domainelocal;
	}
	
	public String getMachine()  
	{
		return this.machine;
	}
	
	public String getDomaine() 
	{
		return this.domainelocal;
	}
	
}
