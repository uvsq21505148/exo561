package exo561;

public class Tuples{
	
	private AdresseIP ip; // NQM = Nom Qualifier de Machine
	private NomMachine nomachine_domainelocal;
	
	public Tuples(AdresseIP a, NomMachine b)
	{
		this.ip = a;
		this.nomachine_domainelocal = b;
	}
		
	public AdresseIP getIP()  
	{
		return this.ip;
	}
	
	public NomMachine getNomMachine() 
	{
		return this.nomachine_domainelocal;
	}
	
}
 
 
 
