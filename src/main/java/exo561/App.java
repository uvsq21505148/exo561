package exo561;

public class App {
	
	public static void main(String args[]) {
		App app = new App();
		app.run();
	}
	
	private Dns serveur;
	private DnsTUI tui;
	
	public App() {
		serveur = new Dns();
		tui = new DnsTUI();
	}
	
	public void run() {
		Commande cmd;
		String resultat;
		
		System.out.println();
		do {
			try {
				cmd = tui.nextCommande();
				resultat = cmd.execute(serveur);
				tui.affiche(resultat);
			}
			catch(NullPointerException e) {
				System.out.println("Commande invalide");
				resultat = "exceptionNoExit";
			}
		} while(!resultat.equals("exit"));
	}
}
