package exo561;

import java.util.Comparator;

public class TuplesComparator implements Comparator<Tuples> {
	
	private boolean optionTri;
	
	public TuplesComparator(boolean optionTri) {
		this.optionTri = optionTri;
	}
	
	public int compare(Tuples t1, Tuples t2) {
		if(optionTri) {
			return t1.getIP().getName().compareTo(t2.getIP().getName());
		}
		else {
			return t1.getNomMachine().getName().compareTo(t2.getNomMachine().getName());
		}
	}
}
