package exo561;

public class AdresseIP extends DnsItem{
	
	private String IP;
	
	public AdresseIP(String adresseip)
	{
		this.IP = adresseip;
	}
	
	@Override
	public String getName()
	{
		return this.IP;
	}
	
}
