package exo561;

import java.util.ArrayList;

public class ListingParNomDomaine implements Commande {
	
	//true => tri selon adresseIP, false => tri selon nom machine
	private boolean option;
	private String nomDomaine;
	
	public ListingParNomDomaine(String nomDomaine, boolean option){
		this.option = option;
		this.nomDomaine = nomDomaine;
	}
	
	public String execute(Dns serveur) {
		StringBuilder str = new StringBuilder();
		ArrayList<NomMachine> liste = serveur.getItems(nomDomaine, option);
		
		for(int i = 0; i < liste.size(); i++) {
			str.append(liste.get(i).getName());
			if(i + 1 != liste.size()) str.append("\n");
		}
		
		String resultat = str.toString();
		if(resultat.equals(""))
			return "Aucune machine trouvée";
		else
			return resultat;
	}
}
