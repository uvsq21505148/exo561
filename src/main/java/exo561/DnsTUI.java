package exo561;

import java.util.Scanner;

public class DnsTUI {
	
	private Commande currentCmd;
	private Scanner stdin;
	
	public DnsTUI() {
		stdin = new Scanner(System.in);
		currentCmd = null;
	}
	
	public Commande nextCommande() {
		String cmd;
		String[] args;
		
		cmd = stdin.nextLine();
		args = cmd.split(" ");
		
		if(cmd.equals("exit")) {
			currentCmd = new Exit();
		}
		else if(args[0].equals("ls")) {
			if(args.length == 3 && args[1].equals("-a") && args[2].split("\\.").length == 2) {
				currentCmd = new ListingParNomDomaine(args[2], true);
			}
			else if(args.length == 2 && args[1].split("\\.").length == 2) {
				currentCmd = new ListingParNomDomaine(args[1], false);
			}
			else {
				currentCmd = null;
			}
		}
		else {
			args = cmd.split("\\.");
			
			if(args.length == 4) // adresse IP
			{
				currentCmd = new RechercheParAdresseIP(new AdresseIP(cmd));
			}
			else if(args.length == 3) // nom qualifié de machine
			{
				//args[0] = nom machine / args[1] + args[2] = nom domaine
				currentCmd = new RechercheParNomMachine(new NomMachine(args[0], args[1] + "." + args[2]));
			}
			else {
				currentCmd = null;
			}
		}
		
		return currentCmd;
	}
	
	public void affiche(String resultat) {
		if(resultat.equals("exit")) {
			System.out.println("Fermeture de l'application.");
		}
		else {
			System.out.println(resultat);
		}
	}
}
